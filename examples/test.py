"""
Created on 27/06/2014
@author: Carlo Pires <carlopires@gmail.com>
"""
from utils import gencode, valid_number

def test1():
    return gencode('a', 'b') # raises TypeCheckError
    
def test2():
    return gencode(b'a', 'b') # no error

def test3():
    return gencode(b'a', b'b') # raises TypeCheckError

def test4():
    return valid_number(2.4) # raises TypeCheckError

def test5():
    import decimal
    return valid_number(decimal.Decimal('2.4')) # no error
    
if __name__ == '__main__':
    import sys
    if len(sys.argv) == 2:
        test = getattr(sys.modules[__name__], sys.argv[1], None)
        if test: 
            print(test())
            exit(0)
    print('Use: {} test1|test2|test3|test4|test5'.format(sys.argv[0]))
