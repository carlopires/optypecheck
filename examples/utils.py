"""
Created on 27/06/2014
@author: Carlo Pires <carlopires@gmail.com>
"""
def gencode(a: bytes, b: str) -> str:
    return '{}{}'.format(a[0], b)

def valid_number(n) -> 'decimal.Decimal':
    return n

assert __import__('typecheck').typecheck(__name__) 
