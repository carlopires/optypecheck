"""
Created on 27/06/2014
@author: Carlo Pires <carlopires@gmail.com>
"""
import unittest
from typecheck import typecheck, Sub, TypeCheckError
from typecheck.types import NoneCls, TupleCls

class Calc:
    TYPE = 'numbers.Real'
    
    def division(self, a: int, b: int) -> float:
        return a/b
    
    def wrong_result(self) -> NoneCls:
        return True
    
    def clone(self) -> 'test.Calc':
        return self.__class__()

    def invalid_clone(self) -> 'test.Calc':
        return self.__class__
    
    def rmul(self, v1: 'self.TYPE', v2: 'self.TYPE') -> 'self.TYPE':
        return v1*v2
    
    def clone_cls(self, ok) -> Sub('test.Calc'):
        class clone(Calc):
            pass
        return clone if ok else None

def average(a: int, b: int) -> float:
    return (a+b)/2

def sqrt(n, exp):
    return n**exp

def wrong_result() -> bool:
    pass

def resolve(n) -> 'decimal.Decimal':
    return n

def create_names() -> TupleCls:
    return tuple(['__priority', '__common'])

assert typecheck(__name__)

class TypecheckTest(unittest.TestCase):
    def test_function_ok(self):
        self.assertEqual(average(1,2), 1.5)
        
    def test_function_with_wrong_arg(self):
        with self.assertRaises(TypeCheckError):
            average(1, 2.0)
        
        with self.assertRaises(TypeCheckError):
            average(2.0, 1)

        with self.assertRaises(TypeCheckError):
            average(1, None)
            
    def test_function_with_wrong_result(self):
        with self.assertRaises(TypeCheckError):
            wrong_result()

    def test_function_with_resolve_ok(self):
        import decimal
        d = decimal.Decimal('2.4')
        self.assertEqual(d, resolve(d))

    def test_function_with_resolve_wrong(self):
        with self.assertRaises(TypeCheckError):
            resolve(2.4)

    def test_function_with_tuple(self):
        self.assertIsInstance(create_names(), type(tuple()))

    def test_method_ok(self):
        c = Calc()
        self.assertEqual(c.division(2, 2), 1)
        
    def test_method_with_wrong_arg(self):
        c = Calc()
        with self.assertRaises(TypeCheckError):
            c.division(2.0, 1)
        
        with self.assertRaises(TypeCheckError):
            c.division(1, 2.0)

        with self.assertRaises(TypeCheckError):
            c.division(1, None)
            
    def test_method_with_wrong_result(self):
        c = Calc()
        with self.assertRaises(TypeCheckError):
            c.wrong_result()

    def test_method_with_resolve(self):
        c = Calc()
        self.assertEqual(c.__class__, c.clone().__class__)
        
    def test_method_with_resolve_and_error(self):
        c = Calc()
        with self.assertRaises(TypeCheckError):
            c.invalid_clone()
            
    def test_method_with_self_resolve_ok(self):
        c = Calc()
        self.assertEqual(c.rmul(2.0, 2.0), 4.0)
        self.assertEqual(c.rmul(2, 2.4), 4.8) # int isinstance of numbers.Real

    def test_method_with_self_and_error(self):
        c = Calc()
        with self.assertRaises(TypeCheckError):
            c.rmul(2, None)
        
        with self.assertRaises(TypeCheckError):
            c.rmul(2.4, '2')
            
    def test_method_with_subclass_ok(self):
        c = Calc()
        self.assertTrue(issubclass(c.clone_cls(True), Calc))

    def test_method_with_subclass_error(self):
        c = Calc()
        with self.assertRaises(TypeCheckError):
            c.clone_cls(False)
            
def suite():
    loader = unittest.TestLoader()
    suite = unittest.TestSuite()
    suite.addTest(loader.loadTestsFromTestCase(TypecheckTest))
    return suite


